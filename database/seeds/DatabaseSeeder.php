<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
	    User::create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => Hash::make('admin'),
            'role' => 'admin'
        ]);
	    for ($i=1;$i<=10;$i++){
		    User::create([
			    'name' => 'Admin_'.$i,
			    'email' => 'admin_'.$i.'@test.com',
			    'password' => Hash::make('admin'),
			    'role' => 'admin'
		    ]);
        }
	    // $this->call(UsersTableSeeder::class);
    }
}
