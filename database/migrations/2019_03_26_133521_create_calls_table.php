<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->unsignedTinyInteger('status')->default(0);
//	        0 - no answer
//	        1 - answer success
//	        2 - call end
	        $table->unsignedTinyInteger('type')->default(0);
//	        0 - voice call
//	        1 - video call
	        $table->unsignedInteger('duration')->nullable()->default(null);
	        $table->unsignedInteger('caller_id');
	        $table->foreign('caller_id')
	              ->references('id')
	              ->on('users')
	              ->onDelete('cascade')
	              ->onUpdate('cascade');
	        $table->unsignedInteger('called_id');
	        $table->foreign('called_id')
	              ->references('id')
	              ->on('users')
	              ->onDelete('cascade')
	              ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
