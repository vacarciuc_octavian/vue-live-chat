<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_files', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->unsignedBigInteger('message_id');
	        $table->foreign('message_id')
	              ->references('id')
	              ->on('messages')
	              ->onDelete('cascade')
	              ->onUpdate('cascade');
			$table->string('name');
			$table->text('path');
			$table->string('type',20);
			$table->string('extension',10);
			$table->bigInteger('size');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_files');
    }
}
