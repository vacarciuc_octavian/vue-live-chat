<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->unsignedBigInteger('conversation_id');
	        $table->foreign('conversation_id')
	              ->references('id')
	              ->on('conversations')
	              ->onDelete('cascade')
	              ->onUpdate('cascade');
            $table->text('body')->nullable();
            $table->unsignedTinyInteger('is_viewed')->default(0);
            $table->unsignedTinyInteger('type')->default(0);
//            0 - text message
//            1 - files message
//            2 - call message, EX: missed call, recent call with duration etc...
//            3 - CV message, if body is a CV-Link
	        $table->unsignedInteger('user_id');
	        $table->foreign('user_id')
	              ->references('id')
	              ->on('users')
	              ->onDelete('cascade')
	              ->onUpdate('cascade');
	        $table->unsignedTinyInteger('related_type')->default(0);
	        // 0 - CV related
	        // 1 - Job related
	        $table->unsignedBigInteger('related_cv_id')->nullable()->default(null);
	        $table->unsignedBigInteger('related_job_id')->nullable()->default(null);
//	        $table->foreign('related_cv_id')
//	              ->references('id')
//	              ->on('cvs')
//	              ->onDelete('set null')
//	              ->onUpdate('set null');
	        $table->unsignedBigInteger('call_id')->nullable()->default(null);
	        $table->unsignedBigInteger('cv_id')->nullable()->default(null);
	        $table->unsignedBigInteger('job_id')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
