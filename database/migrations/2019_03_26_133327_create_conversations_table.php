<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->json('user_types');
	        $table->unsignedBigInteger('cv_id')->nullable()->default(null);
	        $table->unsignedBigInteger('job_id')->nullable()->default(null);
	        $table->unsignedTinyInteger('type')->default(0);
	        // 0 - CV related
	        // 1 - Job related
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
    }
}
