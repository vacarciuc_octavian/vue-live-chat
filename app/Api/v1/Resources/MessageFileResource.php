<?php

namespace App\Api\v1\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageFileResource extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray( $request ) {
		return [
			'id'        => $this->id,
			'name'      => $this->name,
			'path'      => $this->path,
			'type'      => $this->type,
			'extension' => $this->extension,
			'size'      => $this->size,
		];

	}
}
