<?php

namespace App\Api\v1\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class UserResource extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray( $request ) {
//		$auth = Auth::user();
//		$conversation = null;
//		if ( $auth->id !== $this->id ) {
//			$conversation = $this->getOrCreateConversation($auth);
//		}
		return [
			'id'           => $this->id,
			'name'         => $this->name,
			'email'        => $this->email,
			'created_at'   => $this->created_at,
//			'conversation' => $conversation['id']
		];

	}
}
