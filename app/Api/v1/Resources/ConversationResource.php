<?php

namespace App\Api\v1\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ConversationResource extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray( $request ) {
		return [
			'id'           => $this->id,
			'user'         => new UserResource($this->users()->where('users.id','<>',Auth::user()->id)->first()),
			'new'          => $this->new??false,
			'created_at'   => $this->created_at,
		];

	}
}
