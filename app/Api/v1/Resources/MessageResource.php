<?php

namespace App\Api\v1\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray( $request ) {
		return [
			'id'             => $this->id,
			'body'           => $this->body,
			'type'           => $this->type,
			'user'           => new UserResource( $this->user ),
			'conversation'   => $this->conversation['id'],
//			'call'         => $this->call_id,
//			'cv'         => $this->cv_id,
			'files'          => MessageFileResource::collection( $this->files ),
			'related_type'   => $this->related_type,
			'related_cv_id'  => $this->related_cv_id,
			'related_job_id' => $this->related_job_id,
			'created_at'     => $this->created_at,

		];

	}
}
