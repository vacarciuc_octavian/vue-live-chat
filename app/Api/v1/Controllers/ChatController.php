<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Resources\ConversationResource;
use App\Api\v1\Resources\MessageResource;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\MessageFile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class ChatController extends Controller {
	public function __construct() {

		$this->messageRule = [
			'body' => 'string|max:255',
			'type' => 'integer|in:' . implode( ',', Message::$types ),

		];
	}

	public function getConversation( Request $request ) {
		$user   = User::findOrFail( $request->user_id );
		$cv_id  = $request->cv_id ?? null;
		$job_id = $request->job_id ?? null;
		$auth   = Auth::user();
		//TODO normal cv relation
		$conversation = $user->getOrCreateConversation( $auth, $cv_id, $job_id );
		$data         = new ConversationResource( $conversation );

		return response()->json( new \App\Laravue\JsonResponse( $data ) );
	}

	public function getConversations( Request $request ) {
		$auth          = Auth::user();
		$conversations = $auth->conversations()->where( 'user_types->user_' . $auth->id, $auth->current_type )->get();
		$data          = ConversationResource::collection( $conversations );

		return response()->json( new \App\Laravue\JsonResponse( $data ) );
	}

	public function getMessages( Request $request, $conversation_id ) {
		$conversation = Conversation::findOrFail( $conversation_id );
		$messages     = $conversation->messages;
		$data         = MessageResource::collection( $messages );

		return response()->json( new \App\Laravue\JsonResponse( $data ) );
	}

	public function sendMessage( Request $request ) {
		$message = new Message( $request->only( [ 'body' ] ) );
		//TODO check for CV or JOB link message
		//		$message->job_or_cv_link_message()
		//TODO check for CV or JOB link message
		$message->user()->associate( Auth::user() );
		$conversation = Conversation::findOrFail( $request->conversation_id );
		$message->conversation()->associate( $conversation );
		//TODO normal cv/job relation
		$message->related_cv_id  = $conversation->cv_id;
		$message->related_job_id = $conversation->job_id;
		$message->save();
		$message = Message::findOrFail( $message->id );
		if ( $request->hasFile( 'attachments' ) ) {
			foreach ( $request->attachments as $file ) {
				$name       = Storage::disk( 'uploads' )->put( '', $file );
				$extension  = pathinfo( $name, PATHINFO_EXTENSION );
				$attachment = new MessageFile( [
					'name'      => $name,
					'path'      => '/uploads/' . $name,
					'type'      => Message::$types[ $extension ] ?? 'Unknown',
					'extension' => $extension,
					'size'      => $file->getSize(),
				] );
				$attachment->message()->associate( $message )->save();
			}
		}

		\App\Events\Message::dispatch( new \App\Laravue\JsonResponse( new MessageResource( $message ) ), $request->conversation_id );
//		$this->validate($request, $this->rules);
	}

	public static function viewMessages( Request $request ) {
		$conversationId = $request->conversation_id;
		$lastMessageID  = $request->lastMessageId;

		Message::where( 'conversation_id', $conversationId )->where( 'id', '<=', $lastMessageID )->update( [ 'is_viewed' => Message::IS_VIEWED_TRUE ] );

		return response()->status( 201 );

	}
}
