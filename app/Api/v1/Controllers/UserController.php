<?php

namespace App\Api\v1\Controllers;

use App\Api\v1\Resources\UserResource;
use App\Http\Controllers\Controller;
use App\User;


class UserController extends Controller {
	public function __construct( ) {

	}

	public function all( ){
		$users = User::where('id','<>',\Auth::user()->id)->get();

		$data = UserResource::collection( $users );

		return response()->json( new \App\Laravue\JsonResponse( $data ) );
	}
}
