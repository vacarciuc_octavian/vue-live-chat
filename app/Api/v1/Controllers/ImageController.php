<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ImageController extends Controller {
	public function __construct( ) {

		$this->rules = [
			'file' => 'required|image|mimes:jpeg,png,jpg',
		];
	}

	public function uploadImage( Request $request ){
		$this->validate($request, $this->rules);
		if ($request->hasFile('file')) {
			$path = Storage::disk('uploads')->put('', $request->file);
			return response()->json(['location'=>'uploads/'.$path]);
		}
	}
}
