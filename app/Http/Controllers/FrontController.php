<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller {
	public function __construct() {

		$this->data = [
		];
	}

	public function index( Request $request ) {

		return view( 'frontend.index', $this->data );

	}
}