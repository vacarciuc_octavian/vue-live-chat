<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model {

	CONST TYPE_TEXT = 0;
	CONST TYPE_CV = 1;
	CONST TYPE_JOB = 2;
	CONST TYPE_VOICE_CALL = 3;
	CONST TYPE_VIDEO_CALL = 4;

	public static $types = [
		self::TYPE_TEXT       => 'Text message',
		self::TYPE_CV         => 'Cv message',
		self::TYPE_VOICE_CALL => 'Voice call',
		self::TYPE_VIDEO_CALL => 'Video call',
	];

	const RELATED_TYPE_CV = 0;
	const RELATED_TYPE_JOB = 1;
	public static $related_types = [
		self::RELATED_TYPE_CV  => 'CV related',
		self::RELATED_TYPE_JOB => 'Job related',
	];

	const IS_VIEWED_FALSE = 0;
	const IS_VIEWED_TRUE = 1;
	public static $is_viewed_statuses = [
		self::IS_VIEWED_FALSE => 'Not viewed',
		self::IS_VIEWED_TRUE  => 'Viewed',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'body',
		'type',
		'is_viewed',
		'related_type',
		'related_cv_id',
		'related_job_id',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() {
		return $this->belongsTo( User::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function conversation() {
		return $this->belongsTo( Conversation::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function call() {
		return $this->belongsTo( Call::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function files() {
		return $this->hasMany( MessageFile::class );
	}
//	/**
//	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
//	 */
//	public function cv() {
//		return $this->belongsTo( Cv::class );
//	}


}
