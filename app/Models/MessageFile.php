<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageFile extends Model {

	public static $types = [
		'jpg'  => 'image',
		'jpeg' => 'image',
		'png'  => 'image',
		'ico'  => 'image',
		'gif'  => 'image',
		'bmp'  => 'image',
	];
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'path',
		'type',
		'extension',
		'size',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function message() {
		return $this->belongsTo( Message::class );
	}

}
