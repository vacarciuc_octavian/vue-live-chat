<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Call extends Model {

	CONST STATUS_NO_ANSWER  = 0;
	CONST STATUS_ANSWER     = 1;
	CONST STATUS_CALL_END   = 2;
	public static $statuses = [
		self::STATUS_NO_ANSWER => 'No answer',
		self::STATUS_ANSWER    => 'Answer',
		self::STATUS_CALL_END  => 'Call end',
	];

	CONST TYPE_VOICE = 0;
	CONST TYPE_VIDEO = 1;
	public static $types = [
		self::TYPE_VOICE => 'Voice',
		self::TYPE_VIDEO => 'Video',
	];
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'status',
		'type',
		'duration',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function message() {
		return $this->hasOne( Message::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function caller() {
		return $this->belongsTo( User::class );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function called() {
		return $this->belongsTo( User::class );
	}

}
