<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model {

	const TYPE_CV = 0;
	const TYPE_JOB = 1;
	public static $types = [
		self::TYPE_CV  => 'CV related',
		self::TYPE_JOB => 'Job related',
	];

	protected $casts = [
		'user_types' => 'array'
	];
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_types',
		'cv_id',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users() {
		return $this->belongsToMany( User::class, 'conversation_user' );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function messages() {
		return $this->hasMany( Message::class );
	}

}
