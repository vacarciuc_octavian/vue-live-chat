<?php

namespace App\Exports;

use App\Models\PeopleRequests;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PeopleRequestsExport implements FromArray
{
	/**
	 * @return array
	 */
    public function array():array
    {
        return array_merge([[
	        '#','Name','Phone','Email','Country','Visa','Page from','Message','Date',]],
	        PeopleRequests::select(
	        'id','name','phone','email','country','visa','page_from','message','created_at')->get()->toArray());
    }
}
