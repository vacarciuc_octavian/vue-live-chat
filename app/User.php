<?php

namespace App;

use App\Models\Conversation;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject {
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	public function getJWTIdentifier() {
		return $this->getKey();
	}

	public function getJWTCustomClaims() {
		return [];
	}

	public function getOrCreateConversation( $user, $cv_id, $job_id ) {
		$intersectConversations = array_intersect(
			$this->conversations()->where([
				'user_types->user_'.$this->current_type=>$this->current_type,
				'user_types->user_'.$user->current_type=>$user->current_type,
				])->get()->pluck( 'id' )->toArray(),
			$user->conversations()->where([
				'user_types->user_'.$this->current_type=>$this->current_type,
				'user_types->user_'.$user->current_type=>$user->current_type,
			])->get()->pluck( 'id' )->toArray() );
		$conversation_id = array_values($intersectConversations)[0] ?? false;
		if ( ! $conversation_id ) {
			$conversation = Conversation::create( [
				'user_types' => [
					'user_' . $this->id => $this->current_type,
					'user_' . $user->id => $user->current_type,
				]
			] );
			$conversation->users()->sync( [
				$this->id,
				$user->id
			] );
			$conversation->save();
			$new = true;

			//TODO normal cv/job relation
			$conversation->cv_id = $cv_id;
			if($cv_id){
				$conversation->type = Conversation::TYPE_CV;
			}
			$conversation->job_id = $job_id;
			if($job_id){
				$conversation->type = Conversation::TYPE_JOB;
			}
			$conversation->save();
		} else {
			$new = false;
			$conversation = Conversation::find( $conversation_id );
		}
		$conversation->new = $new;
		return $conversation;
	}

	public function conversations() {
		return $this->belongsToMany( Conversation::class, 'conversation_user' );
	}

}
