import Vue from 'vue';
import Router from 'vue-router';
/* Layout */
import Layout from '../views/layout/Layout';
/* Router for modules */
// import leadRouter from './modules/lead';

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router);

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
export const constantRouterMap = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/AuthRedirect'),
    hidden: true,
  },
  {
    path: '/404',
    redirect: { name: 'Page404' },
    component: () => import('@/views/ErrorPage/404'),
    hidden: true,
  },
  {
    path: '/401',
    component: () => import('@/views/ErrorPage/401'),
    hidden: true,
  },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'Dashboard', icon: 'dashboard', noCache: true },
      },
    ],
  },
];

export default new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap,
});
export const asyncRouterMap = [
  {
    path: '/chat',
    component: Layout,
    children: [
      {
        path: '',
        component: () => import('@/views/chat/index'),
        name: 'Chat',
        meta: { title: 'Chat', icon: 'message', noCache: true },
      },
    ],
  },
  // blogRouter,
  // postRouter,
  // languageRouter,
  // {
  //   path: '/contacts',
  //   component: Layout,
  //   children: [
  //     {
  //       path: '',
  //       component: () => import('@/views/contacts/Edit'),
  //       name: 'Contacts',
  //       meta: { title: 'Contacts', icon: 'documentation', noCache: true },
  //     },
  //   ],
  // },
  // {
  //   path: '/peopleRequests',
  //   component: Layout,
  //   children: [
  //     {
  //       path: '',
  //       component: () => import('@/views/peopleRequests/List'),
  //       name: 'PeopleRequests',
  //       meta: { title: 'PeopleRequests', icon: 'documentation', noCache: true },
  //     },
  //   ],
  // },
  { path: '*', redirect: '/404', hidden: true },
];
