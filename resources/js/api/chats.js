import request from '@/utils/request';

export function message(data) {
  return request({
    url: '/message/send',
    method: 'post',
    data: data,
  });
}
export function getMessages(id) {
  return request({
    url: '/messages/' + id,
    method: 'get',
  });
}
export function getConversations(id) {
  return request({
    url: '/conversations',
    method: 'get',
  });
}
export function getConversation(data) {
  return request({
    url: '/conversation',
    method: 'post',
    data: data,
  });
}
