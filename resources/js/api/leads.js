import request from '@/utils/request';

export function fetchList(query) {
  return request({
    url: '/leads',
    method: 'get',
    params: query,
  });
}
export function fetchLead(id) {
  return request({
    url: '/leads/' + id,
    method: 'get',
  });
}
