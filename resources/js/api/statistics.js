import request from '@/utils/request';

export function fetchStatistics() {
  return request({
    url: '/statistics',
    method: 'get',
  });
}
export function fetchCountryStatistics() {
  return request({
    url: '/statistics/country',
    method: 'get',
  });
}
