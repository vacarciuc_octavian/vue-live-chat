<?php
if(isset(\Illuminate\Support\Facades\Request::toArray()['category'])){
	$appendQueryParams = '&category='.\Illuminate\Support\Facades\Request::toArray()['category'];
}else{
	$appendQueryParams = '';
}
if(isset(\Illuminate\Support\Facades\Request::toArray()['search'])){
	$appendQueryParams = '&search='.\Illuminate\Support\Facades\Request::toArray()['search'];
}
?>
@if ($paginator->hasPages())
<div class="block_pagination">
    <nav aria-label="Page navigation example">

        <ul class="pagination">

            @if ($paginator->onFirstPage())
                <li class="page-item extreme" style="opacity: 0;cursor: default;pointer-events: none;">
                    <a class="page-link" disabled="true" rel="prev">Предыдущая страница</a>
                </li>
            @else
                <li class="page-item extreme">
                    <a class="page-link" href="{{$paginator->previousPageUrl().$appendQueryParams }}" rel="prev">Предыдущая страница</a>
                </li>
            @endif

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="page-item">
                                    <a class="page-link number_page active" disabled="true">{{ $page }}</a>
                                </li>
                            @else
                                <li class="page-item">
                                    <a class="page-link number_page" disabled="true" href="{{ $url.$appendQueryParams }}">{{ $page }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="page-item extreme"><a class="page-link" href="{{$paginator->nextPageUrl().$appendQueryParams }}">Следущая страница</a>
                @else
                    <li class="page-item extreme" style="opacity: 0;cursor: default;pointer-events: none;"><a class="page-link" disabled="true">Следущая страница</a>
                @endif
        </ul>
    </nav>
</div>
@endif