<!doctype html>
<html lang="en">
@include('frontend.head')
<body>
@include('frontend.header')
<main>
 @yield('content')
</main>
@include('frontend.footer')
</body>
</html>