<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Call
 *
 * @property int $id
 * @property int $status
 * @property int $type
 * @property int|null $duration
 * @property int $caller_id
 * @property int $called_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $called
 * @property-read \App\User $caller
 * @property-read \App\Models\Message $message
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call whereCalledId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call whereCallerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Call whereUpdatedAt($value)
 */
	class Call extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Conversation
 *
 * @property int $id
 * @property mixed $user_types
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Conversation whereUserTypes($value)
 */
	class Conversation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Message
 *
 * @property int $id
 * @property int $conversation_id
 * @property string $body
 * @property int $type
 * @property int $user_id
 * @property int|null $call_id
 * @property int|null $cv_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Call|null $call
 * @property-read \App\Models\Conversation $conversation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MessageFile[] $files
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereCallId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereConversationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereCvId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereUserId($value)
 */
	class Message extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MessageFile
 *
 * @property int $id
 * @property int $message_id
 * @property string $name
 * @property string $path
 * @property string $type
 * @property string $extension
 * @property int $size
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Message $message
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile whereMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MessageFile whereUpdatedAt($value)
 */
	class MessageFile extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string|null $name
 * @property string $email
 * @property int $current_type
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Conversation[] $conversations
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCurrentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

