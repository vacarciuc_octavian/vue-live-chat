<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::prefix( 'auth' )->group( function () {
	Route::post( 'register', 'AuthController@register' );
	Route::post( 'login', 'AuthController@login' );
	Route::get( 'refresh', 'AuthController@refresh' );
	Route::group( [ 'middleware' => 'auth:api' ], function () {
		Route::get( 'user', 'AuthController@user' );
		Route::post( 'logout', 'AuthController@logout' );
	} );
} );
Route::group( [ 'middleware' => 'auth:api' ], function () {
	Route::post( '/message/send', '\App\Api\v1\Controllers\ChatController@sendMessage');
	Route::get( '/messages/{conversation_id}', '\App\Api\v1\Controllers\ChatController@getMessages');
	Route::get( '/conversations', '\App\Api\v1\Controllers\ChatController@getConversations');
	Route::post( '/conversation', '\App\Api\v1\Controllers\ChatController@getConversation');
	Route::get( '/users', '\App\Api\v1\Controllers\UserController@all');
});
Route::post( '/uploadImage', '\App\Api\v1\Controllers\ImageController@uploadImage' );
